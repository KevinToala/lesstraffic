package com.lesstraffic.geolocationservice.services.carto;

import com.lesstraffic.geolocationservice.model.Geolocation;
import com.lesstraffic.geolocationservice.services.GeolocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Service
@RefreshScope
public class CartoService implements GeolocationService {
	private static final Logger LOGGER = LoggerFactory.getLogger(CartoService.class);
	
	@Value("${lesstraffic.carto.url}")
	private String URL_REST_SERVICE;
	
	@Value("${lesstraffic.carto.api-key}")
	private String CARTO_API_KEY;
	
	@Value("${lesstraffic.carto.table}")
	private String CARTO_TABLE;
	
	@Autowired
	private RestTemplate restTemplate;

	@Override
	public Geolocation insertNode(Geolocation geolocation){
		String insert = String.format(
				"insert into $CartoTable$(the_geom, username, created_date)" +
						" values(ST_SetSRID(ST_Point(%s, %s), 4326), 'ADMIN', '%s')",
				geolocation.getLongitude(), geolocation.getLatitude(),
				Timestamp.valueOf(geolocation.getDate())
		);
		
		try{
			restTemplate.postForObject(buildUrl(insert), null, String.class);
			LOGGER.debug("Geolocalizacion insertada en CartoDB " + geolocation.toString());
		}
		catch(RestClientResponseException restClientResponseException){
			LOGGER.error(
					"Error al llamar a cliente CartoDB\n" +
					"Response:" + restClientResponseException.getResponseBodyAsString(),
					restClientResponseException
			);
		}
		
		return geolocation;
	}

	private URI buildUrl(String query){
		String finalQuery = query.replace("$CartoTable$", CARTO_TABLE);

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(URL_REST_SERVICE)
				.queryParam("q", finalQuery)
				.queryParam("api_key", CARTO_API_KEY);

		return builder.build(false).toUri();
	}
}
