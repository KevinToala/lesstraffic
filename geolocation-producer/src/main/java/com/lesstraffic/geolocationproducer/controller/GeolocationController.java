package com.lesstraffic.geolocationproducer.controller;

import com.lesstraffic.geolocationproducer.dto.GeolocationDTO;
import com.lesstraffic.geolocationproducer.service.GeolocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class GeolocationController {
	private final static Logger LOGGER = LoggerFactory.getLogger(GeolocationController.class);
	
    @Autowired
    private GeolocationService geolocationService;

    @PostMapping("enqueueNode")
    public ResponseEntity<GeolocationDTO> enqueueNode(@RequestBody @Valid GeolocationDTO geolocation){
    	LOGGER.debug("Insertando en cola la geolocalizacion " + geolocation.toString());
    	
        geolocationService.enqueueNode(geolocation);

        return ResponseEntity
                .ok(geolocation);
    }
    
    @GetMapping("hello")
	public ResponseEntity<String> sayHello(){
    	return ResponseEntity.ok("hello");
    }
	
	@GetMapping("hello/{name}")
	public ResponseEntity<String> sayHelloWithName(@PathVariable String name){
		return ResponseEntity.ok("hello " + name);
	}
}
